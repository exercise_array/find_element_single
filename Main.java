import java.util.*;


public class Main {

    /**
     * Find the element that appears once in an array where every other element appears twice
     * Source: https://www.geeksforgeeks.org/find-element-appears-array-every-element-appears-twice/
     */

    public static void main(String[] args) {
        int[] arr = {1, 3, 2, 7, 1, 3, 4, 9, 5, 6, 6, 7, 2, 8, 8, 4, 9};
        System.out.println("Value single is: " + findElementSingle_Loop(arr, arr.length));
        System.out.println("Value single is: " + findElementSingle_Sort(arr, arr.length));
        System.out.println("Value single is: " + findElementSingle_Xor(arr, arr.length));
        System.out.println("Value single is: " + findElementSingle_Hashing(arr, arr.length));
        System.out.println("Value single is: " + findElementSingle_Formula(arr, arr.length));
    }

    public static int findElementSingle_Loop(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; j++) {
                if (arr[i] == arr[j] && i != j)
                    count++;
            }
            if (count == 0) {
                return arr[i];
            }
        }
        return -1;
    }

    public static int findElementSingle_Sort(int[] arr, int n) {

        Arrays.sort(arr);
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] == arr[i + 1]) {
                count++;
            } else {
                if (count == 0)
                    return arr[i];
                else count = 0;
            }
        }
        return -1;
    }

    public static int findElementSingle_Xor(int[] arr, int n) {
        int res = arr[0];
        for(int i = 1; i < n; i++) {
            res = res ^ arr[i];
        }
        return res;
    }

    public static int findElementSingle_Hashing(int[] arr, int n) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int value: arr) {
            if (map.containsKey(value)) {
                map.replace(value, map.get(value) + 1);
            } else {
                map.put(value, 1);
            }
        }
        for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1){
                return entry.getKey();
            }
        }
        return -1;
    }

    // 2 * (a + b + c + d) - (a + a + b + b + c + c + d) = d
    public static int findElementSingle_Formula (int[] arr, int n) {
        Set set = new HashSet();
        for(int value : arr)
            set.add(value);
        int dupSumSignle = 0;
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()){
            dupSumSignle += iterator.next();
        }
        dupSumSignle *= 2;

        int sumArr = 0;
        for(int value : arr)
            sumArr += value;
        return dupSumSignle - sumArr;
    }
}
